<?php 
	session_start();
	$pageName = "Login Template";
	include 'includes/header.php';
?>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4" style="margin-top: 15%">
				<h1>Login</h1>
				
				<?php include 'includes/notifications.php';?>

				<form class="form-horizontal" name="login-form" method="post" action="login.php">
					<div class="form-group">
						<label>User Name: </label>
						<input type="text" name="user_name" class="form-control" id="user_name" required />
					</div>
					<div class="form-group">
						<label>Password: </label>
						<input type="password" name="password" class="form-control" id="password" required />
					</div>
					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-primary" value="Login" id="submit" />
					</div>
				</form>
			</div>
		</div>
	</div>
<?php include 'includes/footer.php';?>