<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-22 09:00:54
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
$pageName = "Broadway || Users";
include 'includes/header.php';
include 'includes/notifications.php';
?>
<style>
ul {
    list-style: none;
}
li {
    float: left;
    margin-right: 10px;
    border: 1px solid #ccc;
    padding: 10px;
    font-size: larger;
    background-color: #ccc;
    border-radius: 10px;
    cursor: pointer;
}
</style>
	<div class="container">
		
		<?php include 'includes/navigation.php'; ?>
		
		<h4>This is User Page</h4>	
		<div class="row">
			<table class="table table-responsive table-bordered">
				<thead>
					<th>S.N.</th>
					<th>Full Name</th>
					<th>Action</th>
				</thead>
				<tbody>
				<?php
					$users= getAllUser();
					if($users){
						$i = 0;
						foreach($users as $user){
							$i++;
					?>
						<tr>
							<td><?php echo $i;?></td>
							<td><?php echo $user['full_name'];?></td>
							<td>
								<a href="">Edit</a>&nbsp;
								<a href="">Delete</a>&nbsp;
							</td>
						</tr>
					<?php
						
						}
				?>
	
					<?php } else {
						?>
							<tr>
								<td colspan="3">There is no any user information in the database.</td>
							</tr>
						<?php
						}?>
				</tbody>
			</table>
		</div>

	</div>
<?php
	include 'includes/footer.php';
?>
