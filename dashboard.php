<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-22 09:00:54
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
$pageName = "Broadway || Dashboard";
include 'includes/header.php';
include 'includes/notifications.php';
?>
<style>
ul {
    list-style: none;
}
li {
    float: left;
    margin-right: 10px;
    border: 1px solid #ccc;
    padding: 10px;
    font-size: larger;
    background-color: #ccc;
    border-radius: 10px;
    cursor: pointer;
}
</style>
	<div class="container">
		
		<?php include 'includes/navigation.php'; ?>
		
		<h4>This is Dashboard</h4>	
		<div class="row">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, reprehenderit, tempora. Fugit, sit laudantium asperiores! Voluptate illum, iusto harum iure aut odit impedit sapiente. Expedita, earum recusandae qui corporis quo!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, est harum, voluptas laborum commodi illum ipsam aperiam ipsum rerum vero. Eaque consequuntur veniam eum libero cupiditate corporis, ratione, culpa. Aliquam!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit dignissimos quis enim asperiores ut ratione fuga saepe iste inventore quod. Beatae perferendis minus id doloremque eveniet! Quibusdam quas ut quasi?</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates, laborum molestiae impedit quia quas quam dolores nemo enim fuga porro voluptatibus ratione eveniet voluptatum rerum ea dolorum dignissimos inventore quis.</p>
		</div>

	</div>
<?php
	include 'includes/footer.php';
?>
