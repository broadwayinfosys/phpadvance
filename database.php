<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-03-06 08:31:29
 * @Organization: Knockout System Pvt. Ltd.
 */
include 'includes/config.php';
include 'includes/functions.php';

$sql = "INSERT INTO  basic_table (full_name, status, added_date) VALUES ('test Name', 1, now())"; //Multiple insertion of data

$sql1 = "INSERT INTO basic_table SET 
			full_name = 'test Name New',
			status = 0,
			added_date = now() "; //One row insert at a time

/*$query = $conn->query($sql);	//mysqli_query
$query1 = $conn->query($sql1);	//mysqli_query, mysqli_fetch_assoc(), mysqli_fetch_array(), mysqli_num_rows(), insert_id, */

// if($query || $query1){
// 	//debugger($conn);
// 	echo "Data inserted into database successfully.";
// } else {
// 	die($conn->error);
// }


//Selecting Data from database

$sql = "SELECT * FROM basic_table";	//Selecting all data from table basic_table

$query = $conn->query($sql);

if($query->num_rows <= 0){
	echo "<br/> No data found in table basic_table";
} else {
	
	$data = array();	//Creating blank array for data
	while($row = $query->fetch_assoc()){	//Fetching individual rows as array
		$data[] = $row;	//Pushing each row on $data
	}

	//debugger($data);	//Debugging $data
}


//Update Query
$sql = "UPDATE basic_table SET full_name ='Full Name Here', status = 0 WHERE id = 14 ";
$query = $conn->query($sql) or die(mysqli_error($conn));
if($query){
	// echo "Row Id 14 updated successfully.";
}

//Delete Query
$sql = "DELETE FROM basic_table WHERE id = 13";
$query = $conn->query($sql) or die(mysqli_error($conn));
if($query){
//	echo "Row id 13 deleted sucessfully";
}
$str = "test user' OR 1=1 ";
$str1 = $conn->real_escape_string($str);
/*echo $str."<br/>";
echo $str1;
*/

$sql = "SELECT * FROM basic_table WHERE full_name = 'Test User'";
$query = $conn->query($sql);
while($row = $query->fetch_assoc()){

}
$name = "Test User";


//Sql Injection Prevention method using prepared statement
$stmt = $conn->prepare('SELECT * FROM basic_table WHERE full_name = ?');
$stmt->bind_param('s',$name);
$stmt->execute();	//Similar to mysqli_query

$result = $stmt->get_result();
//debugger($result,true);
$data = array();
while($row = $result->fetch_assoc()){
	$data[] = $row;
}
debugger($data,true);
?>
