<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-22 09:00:54
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
$pageName = "Broadway || Dashboard";
include 'includes/header.php';
include 'includes/notifications.php';
?>
<style>
ul{
	list-style: none;
}
li {
    float: left;
    margin-right: 10px;
    border: 1px solid #ccc;
    padding: 10px;
    font-size: larger;
    background-color: #ccc;
    border-radius: 10px;
    cursor: pointer;
}
</style>
	<div class="container">
		
		<?php include 'includes/navigation.php'; ?>
		
		<div class="row">
			<h4>This is about us page.</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam labore eos illum. Tempore praesentium omnis esse tempora asperiores dolorum, consequatur vel nihil assumenda consectetur placeat recusandae, nostrum ipsam molestias rem!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam labore eos illum. Tempore praesentium omnis esse tempora asperiores dolorum, consequatur vel nihil assumenda consectetur placeat recusandae, nostrum ipsam molestias rem!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum at ipsam distinctio minima tempora dolores accusamus praesentium molestias sapiente obcaecati neque architecto officiis consequuntur, ex aut inventore enim repellat fugit?</p>
		</div>

	</div>
<?php
	include 'includes/footer.php';
?>
