<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-13 08:40:08
 * @Organization: Knockout System Pvt. Ltd.
 */
?>
<!DOCTYPE html>
<html>
<head>
	<title>File Upload</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="assets/js/jquery-min.3.1.1.js" ></script>
</head>
<body>
	<div class="container">
		<form class="form-horizontal" method="get" action="file-upload-process.php" enctype="multipart/form-data" >
			<input type="checkbox" name="class[]" value="BIM" />BIM &nbsp;
			<input type="checkbox" name="class[]" value="BBS" />BBS &nbsp;
			<input type="checkbox" name="class[]" value="BE" />BE &nbsp;
			<input type="checkbox" name="class[]" value="BBA" />BBA &nbsp;
			
			<div class="form-group">
				<select name="select_class[]" multiple class="form-control">
					<option value="BIM">BIM</option>
					<option value="BBS">BBS</option>
					<option value="BE">BE</option>
					<option value="BBA">BBA</option>
				</select>
			</div> 
			<div class="form-group">
				<label>File Upload</label>
				<input type="file" name="upload[]" accept="images/*,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf" multiple />
			</div>

			<div class="form-group">
				<input type="submit" name="submit" value="Upload" class="btn btn-primary" />
			</div>
		</form>
	</div>
</body>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</html>