<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-06 08:49:30
 * @Organization: Knockout System Pvt. Ltd.
 */

/*Control Statement*/
$bool = true;
$bool1 = false;

$num1 = 4;
$num2 = 5;


if($bool == 1){
	echo "Boolean value is set true";
} else {
	echo '$bool is false';
}

echo "<br />";

if($bool === 1){
	echo "Boolean value is true";
} else {
	echo '$bool is false';
}

echo "<br />";

if($num1 >3 && $num2 <=7){
	echo '$num1 is greater than 3 and $num2 is smaller than or equals to 7 <br/>' ;
	if($num1<5 && $num2>2){
		echo '$num1 is smaller than 5 and $num2 is greater than 2';
	}
} else {
	echo "Above conditions does not satisfy.";
}
echo "<br />";

switch ($num1) {
	case 1:
		echo '$num1 has value 1';
		break;
	case 2:
		echo '$num1 has value 2';
		break;
	case 3:
		echo '$num1 has value 3';
		break;
	case 4:
		echo '$num1 has value 4';
		break;
	case 5:
		echo '$num1 has value 5';
		break;	
	default:
		echo '$num1 has value '.$num1;
		break;
}
echo "<br />";


?>
