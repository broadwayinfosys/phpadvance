<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-01-30 08:48:54
 * @Organization: Knockout System Pvt. Ltd.
 */

	$car = array(array(
					"comp_name" => "Lamborgini",
					"seat_capacity" => 8,
					"engine_capacity" => 1500,
					"model" => array(	"name_1" => "Hurrican",
										"name_2" => 'Aventedor',
										"name_3" => 'Mucilage')),
				array(
					"comp_name" => "Honda",
					"seat_capacity" => 6,
					"engine_capacity" => 1200),
				array(
					"comp_name" => "BMW",
					"seat_capacity" => 8,
					"engine_capacity" => 1800)
				);
	$array1 = array(10,8,4,13,78,42,52,12,1);

//	echo $car[0]['seat_capacity'];
	//echo $car[0]['model']['name_2'];

	//echo $car[0]['seat_capacity'] . "   " . $car[1]['comp_name'];

	/*Array Functions*/
	echo count($car);	//3
	echo "<br/>";
	print_r($array1);
	sort($array1);

	echo "<br/>";
	print_r($array1);

	$array1 = array_reverse($array1);
	echo "<br/>";
	print_r($array1);

	echo "<br/>";
	$first_element = array_shift($array1);
	echo $first_element;

	echo "<br/>";
	print_r($array1);

	echo "<br/>";
	$last_element = array_pop($array1);
	echo $last_element;

	echo "<br/>";
	print_r($array1);

?>
