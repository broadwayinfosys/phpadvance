<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-14 09:11:48
 * @Organization: Knockout System Pvt. Ltd.
 */

//Server

/*echo "<pre>";
print_r($_SERVER);
echo "</pre>";
exit;*/

//Session
session_start();
$user = array(
				"name"=>"Sandesh Bhattarai",
				"organization" => "Knockout System Pvt. Ltd.");

/*$_SESSION['name'] = "Sandesh Bhattarai";
$_SESSION['organization'] = "Knockout System Pvt. Ltd.";*/

$_SESSION['user'] = $user;

echo "<pre>";
print_r($_SESSION);
echo "</pre>";

session_unset();
session_destroy();

echo "<pre>";
print_r($_SESSION);
echo "</pre>";

/*session_unset();
echo "<pre>";
print_r($_SESSION);
echo "</pre>";
exit;*/
?>
