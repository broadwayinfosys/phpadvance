<?php session_start(); ?>
<?php 
	if(isset($_SESSION['uname']) && $_SESSION['uname'] == "admin"){
			$_SESSION['message'] = "You have been already logged in as admin.";
			header('location: home.php');
			exit;
	} else if(isset($_COOKIE['user']) && $_COOKIE['user'] == "admin"){
		$_SESSION['uname'] = $_COOKIE['user'];
		$_SESSION['message'] = "Welcome back.";
		header('location: home.php');
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
	<link rel="shortcut icon" href="upload/06-Contact.jpg" />
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	
	<script type="text/javascript" src="assets/js/jquery-3.1.1.min.js"></script>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4" style="margin-top: 10%">
			<h4>Login</h4>
			<?php 
				echo $_SESSION['message'];
				$_SESSION['message'] = ""; 
			?>
			<form class="form-horizontal" name="login-form" method="post" action="login.php">
			
					<div class="form-group">
						<label>User Name:</label>
						<input type="text" name="username" class="form-control" id="username" required />
					</div>

					<div class="form-group">
						<label>Password:</label>
						<input type="password" name="password" class="form-control" id="password" required />
					</div>
					<div class="pull-left">
						<input type="checkbox" name="remember_me" value="1" /> Remember Me
					</div>
					<div class="form-group pull-right">
						<input type="submit" name="submit" class="btn btn-primary" id="submit" value="Login" />
						<input type="hidden" name="hidden_field" value="This is hidden Value" /> 
					</div>
					
			</form>
		
		</div>
		<div class="col-md-4"></div>
	</div>
</div>

</body>
<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</html>