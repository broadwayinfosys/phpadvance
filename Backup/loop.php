<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-07 08:21:56
 * @Organization: Knockout System Pvt. Ltd.
 */
/*For loop*/
$array = array(
    array("name" => "Ram",
        "class"      => "BIM",
        "roll_no"    => 51),
    array("name" => "Hari",
        "class"      => "BIM",
        "roll_no"    => 52),
    array("name" => "Shyam",
        "class"      => "BBA",
        "roll_no"    => 20),
    array("name" => "Sita",
        "class"      => "BBS",
        "roll_no"    => 101),
    array("name" => "Gita",
        "class"      => "BSCCSIT",
        "roll_no"    => 41),
);

/*echo $array[0]['name'];
echo "<br />";
echo $array[1]['name'];
echo "<br />";
echo $array[2]['name'];
echo "<br />";
echo $array[3]['name'];
echo "<br />";
echo $array[4]['name'];
echo "<br />";
echo "<br />";
echo "<br />";

echo "Using loop";
echo "<br />";
echo "<br />";

for ($i = 0; $i < count($array); $i++) {
	echo $array[$i]['name'];
    echo "<br />";
}

echo "<br />";
echo "Array Using While Loop";
echo "<br />";

$i = 5;
while($i < count($array)){
	echo $array[$i]['name'] . " <br/>";
	$i++;
}

echo "<br />";
echo "Do while loop";
echo "<br />";

$i = 0;
do{
    echo $array[$i]['name'];
    echo "<br />";
	$i++;
} while($i < count($array));
echo "<br />";
echo "<br />";
*/

$array = array(
    array("name" => "Ram",
        "class"      => "BIM",
        "roll_no"    => 51),
    array("name" => "Hari",
        "class"      => "BIM",
        "roll_no"    => 52),
    array("name" => "Shyam",
        "class"      => "BBA",
        "roll_no"    => 20),
    array("name" => "Sita",
        "class"      => "BBS",
        "roll_no"    => 101),
    array("name" => "Gita",
        "class"      => "BSCCSIT",
        "roll_no"    => 41),
);
/*echo "<pre>";
print_r($array);
echo "</pre>";*/

foreach ($array as $key=>$value) {
	//echo $value['name']."<br/>";
}
?>
<table border="1" cellpadding="1" cellspacing="1">
    <thead>
        <th>Name</th>
        <th>Class</th>
        <th>Roll No.</th>
    </thead>
    <tbody>
    
    <?php
        for($i=0; $i<count($array); $i++){
        ?>
        <tr>
            <td><?php echo $array[$i]['name'];?></td>
            <td><?php echo $array[$i]['class'];?></td>
            <td><?php echo $array[$i]['roll_no'];?></td>
        </tr>

        <?php            
        }
    ?>
    </tbody>
</table>


<table border="1" cellpadding="1" cellspacing="1">
    <thead>
        <th>Name</th>
        <th>Class</th>
        <th>Roll No.</th>
    </thead>
    <tbody>
    
    <?php
        foreach($array as $key=>$value){
            echo "<tr>";
            foreach($value as $key1=>$value1){
                echo "<td>".$value1."</td>";
            }
            echo "<tr/>"; 
        }
    ?>
    </tbody>
</table>