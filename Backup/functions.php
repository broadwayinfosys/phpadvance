<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-05 07:37:13
 * @Organization: Knockout System Pvt. Ltd.
 */

//Pre defined Functions
//Date Functions
/*$date = date('Y-m-d'); // Current date 2017-02-21
date_default_timezone_set("Asia/Kathmandu");

echo $date;
echo "<br />";
*/

//Debugger
function debugger($array1){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
	exit;
}


//Add Integer values, Function call by Value
function add_integer($a, $b){
	$sum = $a+$b;

	$a = 9;
	$b = 13;
	
	return $sum;
}

//Function Call By Reference
function add_integer_reference(&$a,&$b){
	$sum = $a+$b;
	
	$a = 9;
	$b = 13;

	return $sum;
}

/*$x = 5;
$y = 7;

$add = add_integer(5,7);  //12
echo "Function Call By Value <br />";
echo $add;
echo "<br/>";
echo $x;
echo "<br/>";
echo $y;
echo "<br/>";


$new_add = add_integer_reference($x,$y);

echo "<hr />Function Call By Reference <br />";
echo $new_add;
echo "<br/>";
echo $x;
echo "<br/>";
echo $y;
echo "<br/>";*/

?>
