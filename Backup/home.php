<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-19 09:21:29
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
if(isset($_SESSION['uname']) && $_SESSION['uname'] == "admin"){
	echo "You are logged in as admin.";
	echo "<br/>";

} else {
	session_unset();
	session_destroy();
	header('location: login-form.php');
}
echo $_SESSION[' '];
$_SESSION['message'] = "";
?>

<a href="logout.php">Logout</a>