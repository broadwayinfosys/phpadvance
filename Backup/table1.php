<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-08 09:15:20
 * @Organization: Knockout System Pvt. Ltd.
 */
$student_info = array(
    array(
        "roll_no"       => 1,
        "name"          => "Student 1",
        "address"       => "Kathmandu",
        "phone_number"  => "9802134567",
        "email_address" => "student1@student.com"),
    array("roll_no" => 2,
        "name"          => "Student 2",
        "address"       => "Lalitpur",
        "phone_number"  => "9802132341",
        "email_address" => "student2@student.com"),
    array("roll_no" => 3,
        "name"          => "Student 3",
        "address"       => "Bhaktapur",
        "phone_number"  => "9849132341",
        "email_address" => "student3@student.com"),
    array("roll_no" => 4,
        "name"          => "Student 4",
        "address"       => "Banepa",
        "phone_number"  => "9870132341",
        "email_address" => "student4@student.com"),
    array("roll_no" => 5,
        "name"          => "Student 5",
        "address"       => "Thankot",
        "phone_number"  => "9874132341",
        "email_address" => "student5@student.com"),
);
?>
<!doctype html>
<html>
	<head>
		<title>Table 1</title>
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
        <script type="text/javascript" src="assets/js/jquery-3.1.1.min.js"></script>
	</head>
	<body>
    <div class="container">
        <div class="row">
            <div class="col-md-3">This is a paragraph</div>
            <div class="col-md-3">This is a paragraph</div>
            <div class="col-md-3">This is a paragraph</div>
            <div class="col-md-3">This is a paragraph</div>
        </div>
        <div class="row">
            <div class="col-md-3">This is second paragraph</div>
            <div class="col-md-3">This is second paragraph</div>
            <div class="col-md-3">This is second paragraph</div>
            <div class="col-md-3">This is second paragraph</div>
        </div>
        <div class="row">
            <div class="col-md-3">This is third paragraph</div>
            <div class="col-md-3">This is third paragraph</div>
            <div class="col-md-3">This is third paragraph</div>
            <div class="col-md-3">This is third paragraph</div>
        </div>
        </div>
	</body>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
</html>
