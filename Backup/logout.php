<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-19 09:28:01
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
session_unset();
session_destroy();
setcookie('user','',time()-360);
header('location: login-form.php');
?>
