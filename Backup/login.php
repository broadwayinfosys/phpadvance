<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-12 08:29:42
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
error_reporting(-1);
$username = "admin";
$password = 'admin';
 

$form_username = (isset($_POST['username']) && $_POST['username'] !="") ? $_POST['username'] : "";

$form_password = (isset($_POST['password']) && $_POST['password'] !="") ? $_POST['password'] : "";


if($username == $form_username ){
	if($password === $form_password){
		$_SESSION['message'] = "You are successfully logged in.";
		$_SESSION['uname'] = $form_username;


		if(isset($_POST['remember_me']) && $_POST['remember_me'] == 1)
		{
			setcookie('user',$form_username,time()+21600);
		}

		header("location: home.php");
	} else {
		$_SESSION['message'] = "Password does not match";
		header('location: login-form.php');
	}
} else {
	$_SESSION['message'] = "Illegal Entry";
	header('location: login-form.php');
}
?>
