<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-27 08:23:50
 * @Organization: Knockout System Pvt. Ltd.
 */

//File Opening
$path = "files";	//Location
if(!file_exists($path) && !is_dir($path)){
	mkdir($path);	//Creating Directory If not exists
}

$file_name = $path."/test.txt";	//Filename 

/*$file = fopen($file_name, 'w') or  die('Error while opening file');	//Opening file in write mode
if($file){
	echo "File Opened for Write mode.";
}
fclose($file);
//file_put_contents($file_name, "");

//File Write
$file = fopen($file_name, 'a') or  die('Error while opening file');	//Opening file in write mode
$data = "This is another test data";

fwrite($file, $data) or die('Error while writing into file');	//Writing Into file
fclose($file);

//File Writing
$data = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum in ea eius iste impedit totam provident, amet nihil! Molestias iste cumque est porro, illum assumenda, optio veniam sequi totam autem!";
file_put_contents($file_name, $data);

$file =fopen($file_name,'r') or die('Error Opening File');
$data = fread($file,filesize($file_name));

fclose($file);

echo "<br />";

$data = file_get_contents($file_name);
echo $data;*/
$array = array(
			array(
				"name" => "Sandesh Bhattarai",
				"email" => "sandesh.bhattarai79@gmail.com",
				"address" => "Kathmandu, Nepal",
				"phone" => 1234567890),
			array(
				"name" => "Test User",
				"email" => "user@user.com",
				"address" => "Kathmandu, Nepal",
				"phone" => 1234567890),
			array(
				"name" => "Another User",
				"email" => "another.user@gmail.com",
				"address" => "Kathmandu, Nepal",
				"phone" => 1234567890),
			array(
				"name" => "Next User",
				"email" => "next.user@gmail.com",
				"address" => "Kathmandu, Nepal",
				"phone" => 1234567890),
			);
//Converting to json object
$json_data = json_encode($array);

$serialize = serialize($array);
$unserialized = unserialize($serialize);
echo "<pre>";
// print_r($unserialized);
echo $serialize;
echo "</pre>";

exit;
//echo $json_data;
//Writing into json Format
file_put_contents($file_name, $json_data);
//
//Reading from file 
$file_data = file_get_contents($file_name);
//echo $file_data;

//Decoding Json Data
$json_decoded_data = json_decode($file_data);

echo $array[3]['name'];

echo "<pre>";
print_r($array);
print_r($json_decoded_data);
echo "</pre>";

echo $json_decoded_data[3]->name;
unlink($file_name);
?>

