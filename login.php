<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-22 08:39:13
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
include 'includes/functions.php';
$username = "admin";
$password = "admin";

$form_username = (isset($_POST['user_name']) && $_POST['user_name'] !="") ? $_POST['user_name'] : "";

$form_password = (isset($_POST['password']) && $_POST['password'] !="") ? $_POST['password'] : "";


if($username == $form_username ){
	if($password === $form_password){
		$_SESSION['success'] = "You are successfully logged in.";
		$_SESSION['uname'] = $form_username;


		if(isset($_POST['remember_me']) && $_POST['remember_me'] == 1)
		{
			setcookie('user',$form_username,time()+21600);
		}

		header("location: dashboard.php");
	} else {
		$_SESSION['error'] = "Password does not match";
		header('location: index.php');
	}
} else {
	$_SESSION['warning'] = "Illegal Entry";
	header('location: index.php');
}

?>
