<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-22 09:26:49
 * @Organization: Knockout System Pvt. Ltd.
 */
	session_start();
	session_destroy();
	$_COOKIE['user'] = "";
	header('location: index.php');
?>
