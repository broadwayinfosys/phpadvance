<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-28 08:55:00
 * @Organization: Knockout System Pvt. Ltd.
 */

include 'includes/functions.php';

$string_1 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti quia repudiandae distinctio similique sit ab reiciendis nobis omnis iure, illo asperiores molestias. Temporibus molestias, asperiores in est. Harum, tenetur repellendus.";

$string_2 = "Hello %s ";
$string_3 = "%s World";

//echo $string_2.$string_3."<br />";

$string = sprintf($string_2,"class");
echo $string;

$string = sprintf($string_3,"Hello");
echo $string;

/*
	String = %s
	Binary = %b
	Number = %u
	ASCII = %c
	Decimal Number = %d
	Scientific notation in lowercase = %e ( 1.2e+2)
	Scientific notation in Uppercase = %E ( 1.2E+2)
*/

//Breaking String with some Delimeter
//$words = explode(" ", $string_1);

//echo count($words)."<br/>" 	;	//Count the number of words

echo str_word_count($string_1); // returns the number of words in a string

//debugger($words);


//03-01-2017
echo "<br/>";
echo $string_1;

$str_split = str_split($string_1,10);
debugger($str_split);	//Splitting a string into chunck of 10 characters

//To find the length of the string.
$string_length = strlen($string_1);
echo $string_length."<br /><br />";


//Joining array elements
$array = array("this","is","array","elements");
$str = implode(" ", $array);
echo $str."<br /><br />";
$str = join(" ", $array);	//Same Functionality as implode()
echo $str."<br /><br />";

//Converting first letter of a string to uppercase
echo ucfirst($str)."<br /><br />";	//First Letter of a string capitalized

echo ucwords($str)."<br /><br />";	//First letter of every word Capitalized

echo strtoupper($str)."<br /><br />"; //Coverting String to Uppercase;

$str2 = "THIS IS ARRAY ELEMENTS";
echo strtolower($str2)."<br /><br />";	//Converting String to lower case

//String Comparison
$str_1 = "This is php class";
$str_2 = "this is php Class";

$str_3 = "this is not php class";
$str_4 = "   THIS IS   NOT   PHP   CLASS";	//White Space
$str_5 = "This is php class";

$comp = strcmp($str_1, $str_2);
echo $comp."<br /><br />";	// output -1

$comp = strcmp($str_2, $str_1);
echo $comp."<br /><br />";	// output 1

$comp = strcmp($str_3, $str_4);
echo $comp."<br /><br />";	// output 1

$comp = strcmp($str_1, $str_5);
echo $comp."<br /><br />";	// output 0


//Removing Whitespaces from a string
//echo $str_4."<br /><br />";	//Without removing spaces

$str_4 = trim($str_4);
echo $str_4."<br /><br />";

//String Patterns
$string = "This is php class. php is a serverside scripting language. php is very easy to learn.";
$pattern = "php";

$pattern_match = strpos($string,$pattern); //First occurance of the pattern or needle from front
echo $pattern_match."<br/><br/>";

$pattern_match_last = strripos($string, $pattern); //Last Occurance of the string pattern
echo $pattern_match_last."<br/><br/>";


//String Replacement
$string_replace = str_replace("php", ".net", $string);
echo $string_replace. "<br/><br/>";

//String Substring
$string = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt libero, aspernatur debitis! Eveniet iusto nesciunt ipsa repudiandae, neque distinctio aperiam unde dolorum. Rem quisquam, cumque officiis. Fuga sint sunt deserunt.";
$sub_string = substr($string, -10,10);	//string, strating point, length
echo $sub_string;

?>
