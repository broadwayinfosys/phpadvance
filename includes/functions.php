<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-02-22 08:51:37
 * @Organization: Knockout System Pvt. Ltd.
 */

function debugger($array,$isDie=false){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
	if($isDie){
		exit;
	}
}

//Function to get All Users information
function getAllUser($isDie=false){
	global $conn;
	$sql = "SELECT * FROM basic_table ORDER BY id DESC";
	if($isDie){
		echo $sql;
		exit;
	}
	$query = $conn->query($sql);
	if($query->num_rows <= 0){
		return false;
	} else {
		$data= array();
		while($row = $query->fetch_assoc()){
			$data[] = $row;
		}
		return $data;
	}
}
?>
