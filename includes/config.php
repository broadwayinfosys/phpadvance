<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-03-06 08:25:30
 * @Organization: Knockout System Pvt. Ltd.
 */
	//Database host, user, password, database name
define('DB_HOST',"localhost");
define('DB_USER',"root");
define('DB_PW',"");
define('DB_NAME','php_advance');

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PW) or die(mysqli_error());
$db = mysqli_select_db($conn,DB_NAME) or die(mysqli_error($conn));

?>