<?php 
if(isset($_SESSION['error']) && $_SESSION['error']!="")
{
	?>
	<!-- Error Message -->
	<div class="alert alert-danger">
		<?php echo $_SESSION['error']; ?>
	</div>
<?php 
} 
$_SESSION['error'] = "";
if(isset($_SESSION['warning']) && $_SESSION['warning']!="")
{
?>
	<!-- Error Message -->
	<div class="alert alert-warning">
		<?php echo $_SESSION['warning']; ?>
	</div>
<?php
} 
$_SESSION['warning'] = "";

if(isset($_SESSION['success']) && $_SESSION['success']!="")
{
?>
	<!-- Error Message -->
	<div class="alert alert-success">
		<?php echo $_SESSION['success']; ?>
	</div>
<?php 
} 
$_SESSION['success'] = "";
?>
<script type="text/javascript">
	setTimeout(function(){
		$('.alert').slideUp();
	},5000)
</script>